class CommentsController < ApplicationController
  
def index
  @comments = Comment.all
end 

def show
end

  def create
    @submit = Submit.find(params[:submit_id])
    @comment = @submit.comments.new(body: comment_params[:body])
  
    if @comment.save
      redirect_to @submit, notice: 'Comment added'
    else
      redirect_to @submit, notice: 'Comment was not saved. Ensure you have entered a comment'
    end
  end

private

def comment_params
  params.require(:comment).permit(:body)
end
end
