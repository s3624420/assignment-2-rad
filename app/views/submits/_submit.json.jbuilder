json.extract! submit, :id, :title, :source, :created_at, :updated_at
json.url submit_url(submit, format: :json)
