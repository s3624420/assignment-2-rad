Rails.application.routes.draw do

 get 'newcomments' => 'comments#index'

  get 'home/index'

  get 'home' => 'home#index' , path: "" 
  resources :submits, except: [:new, :index], path: '/submit' do
    resources :comments, only: [:create, :edit, :update, :destroy]
  end

  get 'submit' => 'submits#new'
  
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
